public class ClassA
{
    public void doA()
    {
        doStep1();
        doStep2();
        doStep3();
    }

    protected void doStep1()
    {
        System.out.println("ClassA doing step 1");
    }

    protected void doStep2()
    {
        System.out.println("ClassA doing step 2");
    }

    protected void doStep3()
    {
        System.out.println("ClassA doing step 3");
    }

}
